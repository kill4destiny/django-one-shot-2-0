from django.shortcuts import render, redirect
from todos.models import TodoList
from todos.forms import TodoForm


def todos_list(request):
    todos = TodoList.objects.all()
    context = {
        'todos_list': todos,
    }
    return render(request, 'todos/list.html', context)


def todos_details(request, id):
    todo = TodoList.objects.get(id=id)
    context = {
        'todos_object': todo
    }
    return render(request, 'todos/details.html', context)


def create_todo(request):
    if request.method == 'POST':
        form = TodoForm(request.POST)
        if form.is_valid():
            todo = form.save(False)
            todo.save()
            return redirect('todos_list')
    else:
        form = TodoForm()

    context = {
        'form': form,
    }
    return render(request, 'todos/create.html', context)


def edit_todo(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == 'POST':
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect('todos_details',id)
    else:
        form = TodoForm(instance=todo)

    context = {
        'todo_object': todo,
        'todo_form': form,
    }
    return render(request, 'todos/edit.html', context)
