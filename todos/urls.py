from django.urls import path
from todos.views import todos_list, todos_details, create_todo, edit_todo


urlpatterns = [
    path('', todos_list, name='todos_list'),
    path('<int:id>/', todos_details, name='todos_details'),
    path('create/', create_todo, name='create_todo'),
    path('<int:id>/edit/', edit_todo, name='edit_todo'),
]
